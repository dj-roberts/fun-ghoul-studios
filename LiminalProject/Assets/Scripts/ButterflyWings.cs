﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButterflyWings : MonoBehaviour
{
    public bool flapping = false;
    private float intensity = 0.0f;
    Transform leftWing;
    Transform rightWing;
    void Awake()
    {
        leftWing = transform.Find("LeftWing");
        rightWing = transform.Find("RightWing");
    }

    // Update is called once per frame
    void Update()
    {
        if (flapping && intensity <= 1.0f)
            intensity = Mathf.Lerp(intensity, 1.0f, Time.deltaTime * 5.0f);
        else if (!flapping && intensity > 0.0f)
            intensity = Mathf.Lerp(intensity, 0.0f, Time.deltaTime * 12.0f);

        Quaternion rot = Quaternion.Euler(0f, Mathf.Lerp(Mathf.Sin(Time.time * 12.0f) * 45.0f * intensity, 15.0f, 1.0f - intensity), 0f);

        leftWing.localRotation = rot;
        rightWing.localRotation = Quaternion.Inverse(rot);
    }

    public void BeginFlap()
    {
        flapping = true;
    }

    public void EndFlap()
    {
        flapping = false;
    }
    
    public void RestInPeaceButterfly()
    {
        Destroy(gameObject);
    }
}
