﻿using UnityEngine;
public class ElasticPopout : MonoBehaviour
{
    private Vector3 scale;
    private Animator animator;
    public float time = 0.0f; // Optional delay in seconds before object pops out, unaffected by speed
    public float speed = 1.0f; // Speed at which the object pops out
    public float intensity = 1.0f; // Intensity of scaling
    void Awake()
    {
        ElasticPopout parentPop = transform.parent.GetComponentInParent<ElasticPopout>();
        if (parentPop) time += parentPop.time; //inherit parent time
        time *= speed;
        scale = transform.localScale;
        transform.localScale = Vector3.zero;
        Animator anim = GetComponentInParent<Animator>();
        if (anim && anim.enabled)
        {
            var id = GetInstanceID(); // account for case where two elastic popouts have the same id
            ElasticPopout searchRoot = parentPop ? parentPop : this;
            bool authorized = true;
            {
                ElasticPopout[] popouts = searchRoot.GetComponentsInChildren<ElasticPopout>();
                Transform parentTransform = this.transform.parent;
                foreach (ElasticPopout pop in popouts)
                {
                    if (pop == this) continue;
                    float t = (pop.transform.parent == parentTransform || pop.transform == parentTransform) ? pop.time : pop.time + time;
                    if (t > this.time || (t == this.time && id < pop.GetInstanceID()))
                    {
                        authorized = false;
                        break;
                    }
                }
            }
            if (authorized)
            {
                anim.enabled = false;
                animator = anim;
            }
        }
    }

    void Update()
    {
        if (time <= -1.0f)
        {
            transform.localScale = scale;
            if (animator) animator.enabled = true;
            Destroy(this);
            return;
        }
        else if (time < 0f)
        {
            float t = Mathf.Pow(2.0f, -10f * -time) * Mathf.Sin((-time - 0.1f) * 5f * Mathf.PI) + 1f;
            float horiz = 1.0f - Mathf.Sin(t * Mathf.PI) * 0.5f;
            if (horiz > 1.0f) horiz = 1.0f + (horiz - 1.0f) * intensity;
            if (t > 1.0f) t = 1.0f + (t - 1.0f) * intensity;
            transform.localScale = Vector3.Scale(Vector3.LerpUnclamped(Vector3.zero, scale, t), new Vector3(horiz, 1f, horiz));
        }
        if (animator) animator.enabled = false;
        time -= Time.deltaTime * speed * 1f;
    }
}
