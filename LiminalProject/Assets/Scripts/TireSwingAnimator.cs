﻿using UnityEngine;

public class TireSwingAnimator : MonoBehaviour
{
    private Quaternion rot;
    private Transform end;
    void Awake()
    {
        rot = transform.localRotation;
        end = transform.Find("End");
    }

    void Update()
    {
        float t = Time.time * 0.75f;
        float n = Mathf.PerlinNoise(t, t * 0.2f) - 0.5f;
        Quaternion noise = Quaternion.Euler(n * 15f, 0f, 0f);
        transform.localRotation = rot * noise;
        end.localRotation = noise;
    }
}
