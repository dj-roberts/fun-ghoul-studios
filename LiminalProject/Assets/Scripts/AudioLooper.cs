﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioLooper : MonoBehaviour
{
    AudioSource audioSource;
    int currentlyPlaying;

    public AudioClip[] clips = new AudioClip[2];
    private double nextEventTime;
    private int flip = 0;
    private AudioSource[] audioSources = new AudioSource[3];
    private bool running = false;

    void Start()
    {
        audioSources = new AudioSource[clips.Length
            ];
        for (int i = 0; i < audioSources.Length; i++)
        {
            audioSources[i] = gameObject.AddComponent<AudioSource>();
        }

        nextEventTime = AudioSettings.dspTime + 2.0f;
        running = true;
    }

    void Update()
    {
        if (!running)
        {
            return;
        }

        double time = AudioSettings.dspTime;

        if (time + 1.0f > nextEventTime)
        {
            audioSources[flip].clip = clips[flip];
            audioSources[flip].PlayScheduled(nextEventTime);
            nextEventTime += audioSources[flip].clip.length;
            flip++;
            if (flip > audioSources.Length - 1)
            {
                flip = 0;
            }
        }
    }
}