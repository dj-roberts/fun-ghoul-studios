﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Rigidbody))]
public class UniversalInteractableComponent : MonoBehaviour, IPointerClickHandler
{
    public UnityEvent OnInteract;
    private ParticleSystem focus;

    private void Awake()
    {
        Transform focusTrans = transform.Find("Focus");
        focus = focusTrans ? focusTrans.GetComponent<ParticleSystem>() : null;
    }
    private void OnTriggerEnter(Collider other)
    {
        OnInteract.Invoke();
        if (focus && !other.GetComponent<ButterflyWings>()) {
            focus.Stop();
            focus = null;
        }
    }
    
    public void OnPointerClick(PointerEventData eventData)
    {
        OnInteract.Invoke();
        if (focus)
        {
            focus.Stop();
            focus = null;
        }
    }
}